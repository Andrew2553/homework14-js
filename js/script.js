const body = document.querySelector('body');
const toggle = document.getElementById('toggle');

window.addEventListener('load', function() {
  const theme = localStorage.getItem('theme');
  if (theme === 'dark') {
    body.classList.add('dark-theme');
    toggle.checked = true;
  }
});

toggle.addEventListener('click', function() {
  if (toggle.checked) {
    localStorage.setItem('theme', 'dark');
    body.classList.add('dark-theme');
  } else {
    localStorage.removeItem('theme');
    body.classList.remove('dark-theme');
  }
});
